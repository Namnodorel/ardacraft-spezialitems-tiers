package me.tobi.ardacraft.spezialitems.scheduler;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.spezialitems.ArdaCraftSpezialItems;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EffectScheduler {
	
	@SuppressWarnings("deprecation")
	public void run() {
		
			final Runnable addEffects = new Runnable() {
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()) {
					Biome b = p.getWorld().getBiome(p.getLocation().getBlockX(), p.getLocation().getBlockZ());
					for(PotionEffect pe : Rasse.get(p).getEffects()) {
						if(pe.getType() != PotionEffectType.ABSORPTION && pe.getType() != PotionEffectType.HEALTH_BOOST)
						p.removePotionEffect(pe.getType());
					}
					if(Rasse.get(p).equals(Rasse.DUNEDAIN)) {
						if(getForestBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
						}else {
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.NIGHT_VISION);
							p.removePotionEffect(PotionEffectType.JUMP);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.ELB)) {
						if(getForestBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.addPotionEffects(Rasse.get(p).getEffects());							
						}
					}else if(Rasse.get(p).equals(Rasse.ENT)) {
						if(getForestBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
						}else {
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.HOBBIT)) {
						if(p.getFoodLevel() > 18) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.REGENERATION);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.removePotionEffect(PotionEffectType.JUMP);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.MAGIER)) {
						if(getPlainBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.WEAKNESS);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.NIGHT_VISION);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.MENSCH)) {
						if(getPlainBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.JUMP);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.NAZGUL)) {
						if(getWaterBiomes().contains(b) || 
								p.getLocation().getBlock().getType() == Material.STATIONARY_WATER ||
								p.getLocation().getBlock().getType() == Material.WATER) {
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.WEAKNESS);
							p.removePotionEffect(PotionEffectType.SLOW);
							p.removePotionEffect(PotionEffectType.CONFUSION);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.ORK)) {
						long time = p.getWorld().getTime();
						if(time > 13000 && time < 23000) {
							//night
							p.removePotionEffect(PotionEffectType.SLOW);
							p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
						}else {
							//day
							p.removePotionEffect(PotionEffectType.NIGHT_VISION);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.OSTLING)) {
						if(p.getFireTicks() > 0) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
						}else {
							p.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.SLOW);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.TROLL)) {
						if(getMountainBiomes().contains(b)) {
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.JUMP);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.URUKHAI)) {
						if(p.getHealth() < 8) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 2));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.WARGREITER)) {
						if(getMountainBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
						}else {
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.removePotionEffect(PotionEffectType.SPEED);
							p.removePotionEffect(PotionEffectType.JUMP);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}else if(Rasse.get(p).equals(Rasse.ZWERG)) {
						if(getMountainBiomes().contains(b)) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
						}else {
							p.removePotionEffect(PotionEffectType.FAST_DIGGING);
							p.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.addPotionEffects(Rasse.get(p).getEffects());
						}
					}
				}
			}
			
		};
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(ArdaCraftSpezialItems.getPlugin(), new Runnable() {

			@Override
			public void run() {
				Bukkit.getScheduler().runTask(ArdaCraftSpezialItems.getPlugin(), addEffects);
			}
			
		}, 200, 200);
		
	}
	
	public List<Biome> getForestBiomes() {
		List<Biome> forest = new ArrayList<Biome>();
		forest.add(Biome.TAIGA_COLD);
		forest.add(Biome.TAIGA_COLD_HILLS);
		forest.add(Biome.MUTATED_TAIGA_COLD);
		forest.add(Biome.TAIGA);
		forest.add(Biome.TAIGA_HILLS);
		forest.add(Biome.MUTATED_TAIGA);
		forest.add(Biome.REDWOOD_TAIGA);
		forest.add(Biome.REDWOOD_TAIGA_HILLS);
		forest.add(Biome.MUTATED_REDWOOD_TAIGA_HILLS);
		forest.add(Biome.MUTATED_TAIGA_COLD);
		forest.add(Biome.FOREST);
		forest.add(Biome.FOREST_HILLS);
		forest.add(Biome.JUNGLE);
		forest.add(Biome.JUNGLE_EDGE);
		forest.add(Biome.MUTATED_JUNGLE_EDGE);
		forest.add(Biome.JUNGLE_HILLS);
		forest.add(Biome.MUTATED_JUNGLE);
		forest.add(Biome.BIRCH_FOREST);
		forest.add(Biome.BIRCH_FOREST_HILLS);
		forest.add(Biome.MUTATED_BIRCH_FOREST);
		forest.add(Biome.MUTATED_BIRCH_FOREST_HILLS);
		forest.add(Biome.ROOFED_FOREST);
		forest.add(Biome.MUTATED_ROOFED_FOREST);
		forest.add(Biome.SAVANNA);
		forest.add(Biome.SAVANNA_ROCK);
		forest.add(Biome.MUTATED_SAVANNA);
		forest.add(Biome.MUTATED_SAVANNA_ROCK);
		forest.add(Biome.MESA_ROCK);
		return forest;
	}
	
	public List<Biome> getMountainBiomes() {
		List<Biome> mountains = new ArrayList<Biome>();
		mountains.add(Biome.TAIGA_COLD_HILLS);
		mountains.add(Biome.MUTATED_TAIGA_COLD);
		mountains.add(Biome.EXTREME_HILLS);
		mountains.add(Biome.MUTATED_EXTREME_HILLS);
		mountains.add(Biome.MUTATED_TAIGA);
		mountains.add(Biome.TAIGA_HILLS);
		mountains.add(Biome.MUTATED_REDWOOD_TAIGA_HILLS);
		mountains.add(Biome.STONE_BEACH);
		mountains.add(Biome.MUTATED_SWAMPLAND);
		mountains.add(Biome.JUNGLE_HILLS);
		mountains.add(Biome.MUTATED_JUNGLE_EDGE);
		mountains.add(Biome.BIRCH_FOREST_HILLS);
		mountains.add(Biome.MUTATED_BIRCH_FOREST_HILLS);
		mountains.add(Biome.MUTATED_BIRCH_FOREST);
		mountains.add(Biome.MUTATED_ROOFED_FOREST);
		mountains.add(Biome.MUTATED_SAVANNA);
		mountains.add(Biome.MUTATED_SAVANNA_ROCK);
		mountains.add(Biome.SAVANNA_ROCK);
		mountains.add(Biome.MESA);
		mountains.add(Biome.MESA_CLEAR_ROCK);
		mountains.add(Biome.MESA_ROCK);
		mountains.add(Biome.MUTATED_MESA);
		mountains.add(Biome.MUTATED_MESA_CLEAR_ROCK);
		mountains.add(Biome.MUTATED_MESA_ROCK);
		return mountains;
	}
	
	public List<Biome> getPlainBiomes() {
		List<Biome> plains = new ArrayList<Biome>();
		plains.add(Biome.MUTATED_ICE_FLATS);
		plains.add(Biome.ICE_FLATS);
		plains.add(Biome.PLAINS);
		plains.add(Biome.MUTATED_PLAINS);
		plains.add(Biome.JUNGLE_EDGE);
		plains.add(Biome.JUNGLE_EDGE);
		plains.add(Biome.MUSHROOM_ISLAND);
		plains.add(Biome.DESERT);
		plains.add(Biome.DESERT_HILLS);
		plains.add(Biome.SAVANNA);
		plains.add(Biome.SAVANNA_ROCK);
		return plains;
	}
	
	public List<Biome> getWaterBiomes() {
		List<Biome> waters = new ArrayList<Biome>();
		waters.add(Biome.FROZEN_RIVER);
		waters.add(Biome.COLD_BEACH);
		waters.add(Biome.STONE_BEACH);
		waters.add(Biome.SWAMPLAND);
		waters.add(Biome.MUTATED_SWAMPLAND);
		waters.add(Biome.RIVER);
		waters.add(Biome.BEACHES);
		waters.add(Biome.MUSHROOM_ISLAND_SHORE);
		waters.add(Biome.MUSHROOM_ISLAND);
		waters.add(Biome.OCEAN);
		waters.add(Biome.DEEP_OCEAN);
		waters.add(Biome.FROZEN_OCEAN);
		return waters;
	}
	
}
