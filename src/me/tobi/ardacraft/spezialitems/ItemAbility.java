package me.tobi.ardacraft.spezialitems;

import me.tobi.ardacraft.spezialabilities.SpezialAbility;

public class ItemAbility {
	
	private SpezialAbility spezialAbility;
	private Integer cooldown;
	private Object parameter;
	
	public ItemAbility(SpezialAbility sa, int cooldownTicks, Object o) {
		setCooldown(cooldown);
		setParameter(o);
		setSpezialAbility(sa);
	}

	public SpezialAbility getSpezialAbility() {
		return spezialAbility;
	}

	public void setSpezialAbility(SpezialAbility spezialAbility) {
		this.spezialAbility = spezialAbility;
	}

	public Integer getCooldown() {
		return cooldown;
	}

	public void setCooldown(Integer cooldown) {
		this.cooldown = cooldown;
	}

	public Object getParameter() {
		return parameter;
	}

	public void setParameter(Object parameter) {
		this.parameter = parameter;
	}
	
}
