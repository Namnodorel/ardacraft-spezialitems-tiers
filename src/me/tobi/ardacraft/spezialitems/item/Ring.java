package me.tobi.ardacraft.spezialitems.item;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.spezialabilities.SpezialAbility;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Ring extends SpezialItem{

	public Ring(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.NONE;
		super.material = Material.DOUBLE_PLANT;
		super.name = "Der Ring";
		super.rassen.add(Rasse.ALLE_RASSEN);
	}
	
	public static List<Player> ringActive = new ArrayList<Player>();
	
	@Override
	public void onRightClick(Player p) {
		if(tier == 0) {
			SpezialAbility.RING.execute(p, 0, 1200);
		}else if(tier == 1) {
			SpezialAbility.RING.execute(p, 1, 1800);		
		}else if(tier == 2) {
			if(ringActive.contains(p)) {
				ringActive.remove(p);
				p.removePotionEffect(PotionEffectType.INVISIBILITY);
				p.removePotionEffect(PotionEffectType.CONFUSION);
				p.removePotionEffect(PotionEffectType.HUNGER);
			}else {
				ringActive.add(p);
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, Integer.MAX_VALUE, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, Integer.MAX_VALUE, 0));
			}
		}else if(tier == 3) {
			if(ringActive.contains(p)) {
				ringActive.remove(p);
				for(Player pl : Bukkit.getOnlinePlayers()) {
					pl.showPlayer(p);
				}
				p.sendMessage("§aDu bist jetzt wieder sichtbar!");
				p.removePotionEffect(PotionEffectType.CONFUSION);
				p.removePotionEffect(PotionEffectType.HUNGER);
				p.removePotionEffect(PotionEffectType.POISON);
			}else {
				ringActive.add(p);
				for(Player pl : Bukkit.getOnlinePlayers()) {
					if(Rasse.get(pl) != Rasse.MAGIER && Rasse.get(pl) != Rasse.NAZGUL && pl != p) {
						pl.hidePlayer(p);
					}
				}
				p.sendMessage("§aDu bist jetzt unichtbar!");
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, Integer.MAX_VALUE, 0));
			}
		}
	}

}
