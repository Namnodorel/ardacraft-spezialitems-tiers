package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.PotionEffectManager;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialitems.ArdaCraftSpezialItems;
import me.tobi.ardacraft.spezialitems.SpezialItem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Set;

public class Menschenschild extends SpezialItem{

	public Menschenschild(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.ATTACK;
		super.material = Material.SHIELD;
		super.name = "Menschenschild";
		super.rassen.add(Rasse.MENSCH);
		super.rassen.add(Rasse.OSTLING);
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) {
		if(hit instanceof LivingEntity) {
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 4, false));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 6, false));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 10, false));
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 14, false));
			}
		}
	}

    @Override
    public void onPlayerGetsHit(EntityDamageByEntityEvent event, Entity attacker, Player hitPlayer) {

        if(tier == 0){
            event.setDamage(event.getDamage() - 1);
        }else if(tier == 1){
            event.setDamage(event.getDamage() - 2);
        }else if(tier == 2){
            event.setDamage(event.getDamage() - 3);
        }else if(tier == 3){

			if(hitPlayer.isBlocking()){

                if(event.getDamage() <= 4){
                    event.setDamage(0);

                    if(hitPlayer.getHealth() < hitPlayer.getMaxHealth()){
                        hitPlayer.setHealth(hitPlayer.getHealth() + 1);
                    }
                }else{
                    event.setDamage(event.getDamage() - 6);
                }

            }else{

                event.setDamage(event.getDamage() - 5);
            }

        }

    }

    private int task;

    private int secondsHolding = 0;

	@Override
	public void onRightClick(final Player p) {


		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(ArdaCraftSpezialItems.getPlugin(), new BukkitRunnable() {

			@Override
			public void run() {

                if (p.isBlocking()) {

					if(tier == 1){
                        System.out.println(p.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE).getBaseValue());
                        p.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE).setBaseValue(1);
                    }

					if (tier == 2) {
						PotionEffectManager.addPotionEffect(p, new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 1));
					}
					if (tier == 3) {
                        PotionEffectManager.addPotionEffect(p, new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 1));

                        secondsHolding++;
                    }
				} else {

                    if(tier == 3){

                        if(secondsHolding >= 10){
                            //Teleport

                            Set<Material> transparentMaterials = null;

                            if(p.getTargetBlock(transparentMaterials, 8) != null){
                                Location teleportTo = p.getTargetBlock(transparentMaterials, 8).getLocation();
                                teleportTo.setY(teleportTo.getY() + 1);
                                p.teleport(teleportTo);
                            }
                        }

                        secondsHolding = 0;
                    }

					try {
						Bukkit.getScheduler().cancelTask(task);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			}

		}, 10, 20);

	}

	
}
