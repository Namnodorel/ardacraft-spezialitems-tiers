package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.spezialabilities.SpezialAbility;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Dolch extends SpezialItem{

	public Dolch(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.ATTACK;
		super.material = Material.STICK;
		super.name = "Dolch";
		super.rassen.add(Rasse.NAZGUL);
		super.rassen.add(Rasse.HOBBIT);
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) { //TODO include dmg
		if(hit instanceof LivingEntity) {
			LivingEntity hittet = (LivingEntity) hit;
			if(tier == 0) {
				hittet.damage(2);
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 160, 0));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 60, 0));
			}else if(tier == 1) {
				hittet.damage(4);
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 1));
			}else if(tier == 2) {
				hittet.damage(5);
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 140, 1));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 0));
			}else if(tier == 3) {
				hittet.damage(7);
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 140, 1));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 0));
			}
		}
	}
	
	@Override
	public void onRightClick(Player p) {
		if(tier == 3) {
			SpezialAbility.DOLCH_ABILITY.execute(p, null, 1200);
		}
	}
	
}
