package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialabilities.SpezialAbility;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Kampfaxt extends SpezialItem{

	public Kampfaxt(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.ATTACK;
		super.material = Material.STONE_AXE;
		super.name = "Kampfaxt";
		super.rassen.add(Rasse.ZWERG);
		super.rassen.add(Rasse.ORK);
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) { //TODO include dmg
		if(hit instanceof LivingEntity) {
			LivingEntity hittet = (LivingEntity) hit;
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 9, false));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 12, false));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 18, false));
				hittet.setFireTicks(100);
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 30, false));
				hittet.setFireTicks(100);
			}
		}
	}
	
	@Override
	public void onRightClick(Player p) {
		if(tier == 3) {
			SpezialAbility.KAMPFAXT_ABILITY.execute(p, null, 1200);
		}
	}

}
