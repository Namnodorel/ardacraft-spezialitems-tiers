package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Keule extends SpezialItem{

	public Keule(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.ATTACK;
		super.material = Material.WOOD_SPADE;
		super.name = "Keule";
		super.rassen.add(Rasse.TROLL);
		super.rassen.add(Rasse.ENT);
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) { //TODO include dmg
		if(hit instanceof LivingEntity) {
			LivingEntity hittet = (LivingEntity) hit;
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 12, false));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 18, false));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 24, false));
				hittet.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 80, 0));
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 48, false));
				attacker.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 1));
				attacker.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 1));
			}
		}
	}

}
